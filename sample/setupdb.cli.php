#! /usr/bin/php
<?php
/**
 * sample table creating for cronjob class
 * @version 2009-11-16 17:49:25 +0100
 * @copyright Martin Pircher <mplx+code@donotreply.at>
 * @author Martin Pircher <mplx+code@donotreply.at>
 * @link http://www.pircher.net/
 * @license http://opensource.org/licenses/MIT MIT License
 * @package Cronjob
 */

use \mplx\toolkit\cronjob\CronJob;

/**
 * Database configuration
 */
include 'config.inc.php';

/**
 * Check for CLI
 */
if (@php_sapi_name() != 'cli') {
    die('ERROR: This script will only work in the shell'.PHP_EOL);
}

/**
 * Include cronjob php class
 */
include dirname(__FILE__).'/../src/cronjob.php';

/**
 * Initialize
 *
 * create object, initialize database connection and create table
 */
$job = new CronJob('samplejob', $dbcfg);
$job->createTable();

<?php
/**
 * sample database configurations
 * @version 2009-11-16 17:49:25 +0100
 * @copyright Martin Pircher <mplx+code@donotreply.at>
 * @author Martin Pircher <mplx+code@donotreply.at>
 * @link http://www.pircher.net/
 * @license http://opensource.org/licenses/MIT MIT License
 * @package Cronjob
 */

/**
 * Database configuration: PDO MySQL
 */
$dbcfg = array(
    'type' => 'mysql',
    'host' => '10.0.0.127',
    'database' => 'cronjob',
    'user' => 'root',
    'password' => '',
    'table' => 'sample_' . 'cronjobs',
    'options' => array(
                    \PDO::ATTR_PERSISTENT => true,
                    \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
                    \PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'UTF8'"
                )
);

/**
 * Database configuration: PDO DSN
 */
$dbcfg = array(
    'type' => 'pdo-dsn',
    'dsn' => 'mysql:host=10.0.0.127;port=3306;dbname=cronjob',
    'user' => 'root',
    'password' => '',
    'table' => 'sample_' . 'cronjobs',
    'options' => array(
                    \PDO::ATTR_PERSISTENT => true,
                    \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
                    \PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'UTF8'"
                )
);

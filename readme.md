PHP Cronjob
===========================================

## Introduction ##

The cronjob php class allows PHP CLI scripts to register themself
to get executed within an to be set timeframe.

You will need crontab or similar to call an scheduler script ("executor")
which reads the jobs to be run and then forks them. A sample executor
using POSIX process control is provided.

The class provides ability to

* register and delete job from execution table
* schedule the current job to a new time
* enable/disable the execution of a job
* locking to prevent multiple execution of same job

For easy handling (installation and autoloader) a composer.json is
included but you also can simple include src/cronjob.php. The class
uses the namespace mplx\toolkit\cronjob.

To allow quick access to most basic functions a basic command
line tool is included.

## Requirements ##

* PHP 5.3+
* Crontab or similar to run scheduler script
* Recommended: Composer

## Getting Started ##

The easiest way to work with php-toolkit-cronjob is to install as a
Composer package inside your project. Composer isn't strictly
required, but makes life a lot easier.

If you're not familiar with Composer, please see <http://getcomposer.org/>.

A sample composer.json is provided below.

## Jobs ##

Documentation (`apigen -s src/,sample/ -d docs/`) is included in docs/.

### (un)register job ###

    public function registerJob($cmd, $interval, $jobid = '');
    public function unregisterJob($jobid = '');

### Reschedule ###

    public function scheduleJob($ts, $jobid = '');

### enable/disable job ###

    public function enableJob($jobid = '');
    public function disableJob($jobid = '');

### locking ###

    public function setLock($jobid = '');
    public function releaseLock($jobid = '');

### status ###

    public function getStatus($jobid = '');
    public function getScheduledJobs();

### helper: creating database table ###

    public function createTable()

## Commandline tool ##

A rudimentary command line tool is included to allow basic
functions to be performed directly from shell.

The first parameter has to be a PHP file with the
required database configuration (see `sample/config.inc.php`).

### create database table ###

    php bin/crontab sample/config.inc.php table create

###enable/disable job ###

    php bin/crontab sample/config.inc.php enable myjob
    php bin/crontab sample/config.inc.php disable myjob

### unlock job ###

    php bin/crontab sample/config.inc.php unlock myjob

### delete job ###

    php bin/crontab sample/config.inc.php delete myjob

### get full status of job ###

    php bin/crontab sample/config.inc.php status myjob

### list all registered jobs ###

    php bin/crontab sample/config.inc.php list
    php bin/crontab sample/config.inc.php list myjob
    php bin/crontab sample/config.inc.php list my%
    php bin/crontab sample/config.inc.php list %job%

## Usage ##

### composer.json ###

    {
       "name": "mplx/sample",
       "license": "proprietary",
       "description": "sample for installing php-toolkit-cronjob with composer",

       "repositories": [
            {
                "type": "vcs",
                "url": "https://mplx@bitbucket.org/mplx/php-toolkit-cronjob.git"
            }
        ],

        "require": {
            "mplx/php-toolkit-cronjob": "dev-master"
        }
    }

### sample usage with (composer) autoloader ###

    <?php

    $dbcfg=array(
        'user' => 'root',
        'password' => '',
        'type' => 'mysql',
        'host' => 'localhost',
        'database' => 'cronjob',
        'table' => 'sample_' . 'cronjobs'
    );

    $loader = require_once 'vendor/autoload.php';

    use mplx\toolkit\cronjob;

    $job = new CronJob('sched', $dbcfg);
    ...

### sample usage without autoloader ###

    <?php

    $dbcfg=array(
        'user' => 'root',
        'password' => '',
        'type' => 'mysql',
        'host' => 'localhost',
        'database' => 'cronjob',
        'table' => 'sample_' . 'cronjobs'
    );

    include dirname(__FILE__).'/vendor/mplx/php-toolkit-cronjob/src/cronjob.php';

    $job = new mplx\toolkit\cronjob\CronJob('sched', $dbcfg);
    ...

### sample job/executor ###

In /sample directory you'll find samples for

- simple PHP CLI script to be called (`samplejob.cli.php`)
- simple schedular script to be called by cron every few minutes (`executor.cron.php`)

To be able to run them you'll have edit the database credentials (`config.inc.php`)
and create the cronjob table (`setupdb.cli.php`).

You can run the job scheduler every 2 minutes from cron.

    # php cronjob scheduler
    MAILTO=dummy@dummy.tld
    *2 * * * *    root    /usr/bin/php /var/www/.../toolkit-cronjob/sample/executor.cron.php

## PHP Framework Interoperability Group standards recommendation (PSR) ##

Some effort has been made to refactore the old code to PSR compliance. The code
has been tested with PHP CodeSniffer (`phpcs --standard=PSR2 src/ sample/`). For more
information on PSR standards visit <https://github.com/php-fig/fig-standards>.

<?php
/**
 * Class loader - when used without PSR-0 compatible autoloader
 * @copyright Martin Pircher <mplx+code@donotreply.at>
 * @author Martin Pircher <mplx+code@donotreply.at>
 * @link http://www.pircher.net/
 * @license http://opensource.org/licenses/MIT MIT License
 * @package Cronjob
 */

/**
* Load interface
*/
require_once dirname(__FILE__).'/mplx/toolkit/cronjob/CronJobInterface.php';

/**
* Load abstract database class
*/
require_once dirname(__FILE__).'/mplx/toolkit/cronjob/CronJobDBAbstract.php';

/**
* Load database class implemented with PDO
*/
require_once dirname(__FILE__).'/mplx/toolkit/cronjob/CronJobDBPDO.php';

/**
* Load cronjob class implementing the interface
*/
require_once dirname(__FILE__).'/mplx/toolkit/cronjob/CronJob.php';

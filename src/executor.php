<?php
/**
 * executes scheduled cronjobs
 * @version 2009-11-16 17:49:25 +0100
 * @copyright Martin Pircher <mplx+code@donotreply.at>
 * @author Martin Pircher <mplx+code@donotreply.at>
 * @link http://www.pircher.net/
 * @license http://opensource.org/licenses/MIT MIT License
 * @package Cronjob
 */

/**
 * Check for CLI
 */
if (@php_sapi_name() != 'cli') {
    die('ERROR: This script will only work in the shell'.PHP_EOL);
}

/**
 * PCNTL available?
 */
if (! function_exists('pcntl_fork')) {
    $error = "ERROR: ".
                "this scheduler uses PHP's Process Control extension ".
                "which seems not be enabled/available on your system.'".PHP_EOL;
    die ($error);
}

/**
 * Check for database configuration
 */
if (! isset($dbcfg)) {
    die('ERROR: Missing database configuration'.PHP_EOL);
}

/**
 * Cronjob class
 **/
include dirname(__FILE__).'/cronjob.php';

/**
 * Get all jobs to be processed
 **/
$sched = new \mplx\toolkit\cronjob\CronJob('sched', $dbcfg);
$jobs = $sched->getScheduledJobs();
unset($sched);

if (! $jobs) {
    die(''); /*nothing to do*/
}

/**
 * fork child for each job
 **/
foreach ($jobs as $job) {
    $pid = pcntl_fork();
    if (! $pid) {
        $descriptorspec = array(
            0 => array("pipe", "r"),
            1 => array("pipe", "w"),
            2 => array("pipe", "w")
        );
        $process = proc_open($job['cmd'], $descriptorspec, $pipes);
        if (is_resource($process)) {
            echo $procout = stream_get_contents($pipes[1]);
            echo $procerror = stream_get_contents($pipes[2]);
            fclose($pipes[1]);
            fclose($pipes[2]);
            $return_value = proc_close($process);
        }
        break;
    }
}

<?php
/**
 * Abstract class for database functions
 * @version 2009-11-16 17:49:25 +0100
 * @copyright Martin Pircher <mplx+code@donotreply.at>
 * @author Martin Pircher <mplx+code@donotreply.at>
 * @link http://www.pircher.net/
 * @license http://opensource.org/licenses/MIT MIT License
 * @package Cronjob
 */

namespace mplx\toolkit\cronjob;

/**
 * Abstract class for database functions
 */
abstract class CronjobDBAbstract
{
    /**
     * Empty constructor to avoid bug in PHP 5.3.2
     */
     public function __construct() {}

    /**
     * Initialize database connection
     *
     * @param mixed $cfg
     */
    abstract protected function initializeDB($cfg);

    /**
     * Register new cronjob
     *
     * @param string $jobid unique job identifier
     * @param int $interval interval in seconds
     * @param string $cmd commandline
     */
    abstract protected function registerJobDB($jobid, $interval, $cmd);

    /**
     * Unregister (delete) cronjob
     *
     * @param string $jobid unique job identifier
     */
    abstract protected function unregisterJobDB($jobid);

    /**
     * Enable/Disable cronjob
     *
     * @param string $jobid unique job identifier
     * @param bool $enabled enable(true) or disable(false)
     */
    abstract protected function setEnabledDB($jobid, $enabled);

    /**
     * Lock/Unlock cronjob
     *
     * @param string $jobid unique job identifier
     * @param bool $locked lock(true) or unlock(false)
     */
    abstract protected function setLockDB($jobid, $locked);

    /**
     * Schedule next execution
     *
     * @param string $jobid unique job identifier
     * @param mixed $ts timestamp
     */
    abstract protected function scheduleJobDB($jobid, $ts);

    /**
     * Retrieve cronjob status
     *
     * @param string $jobid unique job identifier
     */
    abstract protected function getJobStatusDB($jobid);

    /**
     * Retrieve list of all cronjobs
     *
     * @param mixed $flags filtering with custom sql WHERE clause
     * @param mixed $sort sorting with custom sql ORDER BY
     */
    abstract protected function getJobsDB($flags = '', $sort = '');
}
// end abstract class CronjobDBAbstract

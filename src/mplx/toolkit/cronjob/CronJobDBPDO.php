<?php
/**
 * Class implementing database functions: PDO MySQL
 * @version 2009-11-16 17:49:25 +0100
 * @copyright Martin Pircher <mplx+code@donotreply.at>
 * @author Martin Pircher <mplx+code@donotreply.at>
 * @link http://www.pircher.net/
 * @license http://opensource.org/licenses/MIT MIT License
 * @package Cronjob
 */

namespace mplx\toolkit\cronjob;

use PDO;
use PDOException;

/**
 * Class implementing database functions: PDO MySQL
 */
class CronJobDBPDO extends CronjobDBAbstract
{
    private $dbh;
    private $table;

    /**
     * Initialize database connection
     *
     * {@inheritdoc}
     */
    protected function initializeDB($cfg)
    {
        if ($cfg['type'] == 'pdo-dsn') {
            $dsn = $cfg['dsn'];
        } elseif ($cfg['type'] == 'mysql') {
            $dsn = $cfg['type'] . ':dbname=' . $cfg['database'] . ';host=' . $cfg['host'];
        } else {
            die('ERROR: database initialization failed - unhandled database type');
        }
        try {
            $this->dbh = new PDO(
                $dsn,
                $cfg['user'],
                $cfg['password'],
                $cfg['options']
            );
            $this->table = $cfg['table'];
        } catch (PDOException $e) {
            $this->dbh = null;
            throw new \Exception('database connection failed');
            die();
        }
    }

    /**
     * Creates database table to hold cronjobs
     *
     * helper function for easy creation of database table
     */
    public function createTable()
    {
        $dbquery = "CREATE TABLE IF NOT EXISTS `" . $this->table . "` (".
                    "`jobid` VARCHAR(20) NOT NULL, ".
                    "`cmd` VARCHAR(255) NOT NULL, ".
                    "`enabled` ENUM('n','y') NOT NULL DEFAULT 'n', ".
                    "`locked` ENUM('n','y') NOT NULL DEFAULT 'n', ".
                    "`interval` MEDIUMINT(5) UNSIGNED NOT NULL, ".
                    "`created` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00', ".
                    "`scheduled` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00', ".
                    "`lastexecution` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00', ".
                    "PRIMARY KEY (`jobid`)".
                    ") COLLATE=utf8_general_ci";
        try {
            $sth = $this->dbh->prepare($dbquery);
            return $sth->execute();
        } catch (PDOException $e) {
            throw new \Exception('create table failed');
            die();
        }
    }

    /**
     * Register new cronjob
     *
     * {@inheritdoc}
     */
    protected function registerJobDB($jobid, $interval, $cmd)
    {
        $dbquery = "INSERT INTO " . $this->table . " ".
                    "(jobid,cmd,`interval`,created) VALUES ".
                    "(:jobid,:cmd,:interval,NOW())";
        try {
            $sth = $this->dbh->prepare($dbquery);
            $sth->bindValue(':jobid', $jobid, PDO::PARAM_STR);
            $sth->bindValue(':cmd', $cmd, PDO::PARAM_STR);
            $sth->bindValue(':interval', $interval, PDO::PARAM_INT);
            return $sth->execute();
        } catch (PDOException $e) {
            throw new \Exception('registration of new job failed');
            die();
        }
    }

    /**
     * Unregister (delete) cronjob
     *
     * {@inheritdoc}
     */
    protected function unregisterJobDB($jobid)
    {
        $dbquery = "DELETE FROM " . $this->table . " WHERE jobid=:jobid";
        try {
            $sth = $this->dbh->prepare($dbquery);
            $sth->bindValue(':jobid', $jobid, PDO::PARAM_STR);
            $result = $sth->execute();
            return ($sth->rowCount() <> 1 ? false : true);
        } catch (PDOException $e) {
            throw new \Exception('deregistration of job failed');
            die();
        }
    }

    /**
     * Enable/Disable cronjob
     *
     * {@inheritdoc}
     */
    protected function setEnabledDB($jobid, $enabled)
    {
        if ($enabled) {
            $flag='y';
        } else {
            $flag='n';
        }
        $dbquery="UPDATE " . $this->table . " SET enabled=:flag WHERE jobid=:jobid";
        try {
            $sth = $this->dbh->prepare($dbquery);
            $sth->bindValue(':jobid', $jobid, PDO::PARAM_STR);
            $sth->bindValue(':flag', $flag, PDO::PARAM_STR);
            $result = $sth->execute();
            return ($sth->rowCount() <> 1 ? false : true);
        } catch (PDOException $e) {
            throw new \Exception('updating job failed');
            die();
        }
    }

    /**
     * Lock/Unlock cronjob
     *
     * {@inheritdoc}
     */
    protected function setLockDB($jobid, $locked)
    {
        if ($locked) {
            $flag = 'y';
        } else {
            $flag = 'n';
        }
        $dbquery="UPDATE ".$this->table." SET locked=:flag, lastexecution=NOW() WHERE jobid=:jobid";
        try {
            $sth = $this->dbh->prepare($dbquery);
            $sth->bindValue(':jobid', $jobid, PDO::PARAM_STR);
            $sth->bindValue(':flag', $flag, PDO::PARAM_STR);
            $result = $sth->execute();
            return ($sth->rowCount() <> 1 ? false : true);
        } catch (PDOException $e) {
            throw new \Exception('updating job failed');
            die();
        }
    }

    /**
     * Schedule next execution
     *
     * {@inheritdoc}
     */
    protected function scheduleJobDB($jobid, $ts)
    {
        $dbquery = "UPDATE " . $this->table . " SET scheduled=FROM_UNIXTIME(:scheduled) WHERE jobid=:jobid";
        try {
            $sth=$this->dbh->prepare($dbquery);
            $sth->bindValue(':jobid', $jobid, PDO::PARAM_STR);
            $sth->bindValue(':scheduled', $ts, PDO::PARAM_INT);
            $result = $sth->execute();
            return ($sth->rowCount() <> 1 ? false : true);
        } catch (PDOException $e) {
            throw new \Exception('updating job failed');
            die();
        }
    }

    /**
     * Retrieve cronjob status
     *
     * {@inheritdoc}
     */
    protected function getJobStatusDB($jobid)
    {
        $dbquery = "SELECT * FROM " . $this->table . " WHERE jobid=:jobid";
        try {
            $sth = $this->dbh->prepare($dbquery);
            $sth->bindValue(':jobid', $jobid, PDO::PARAM_STR);
            if ($sth->execute()) {
                return $sth->fetch(PDO::FETCH_ASSOC);
            } else {
                return false;
            }
        } catch (PDOException $e) {
            throw new \Exception('database query failed');
            die();
        }
    }

    /**
     * Retrieve list of all cronjobs
     *
     * {@inheritdoc}
     */
    protected function getJobsDB($flags = '', $sort = '')
    {
        $filter = '';
        $orderby = '';

        if ($flags) {
            foreach ($flags as $flag => $vflag) {
                if ($filter != '') {
                    $filter .= ' AND ';
                }
                $filter .= $vflag;
            }
        }

        $dbquery = "SELECT * FROM " . $this->table;
        if ($filter) {
            $dbquery .= ' WHERE ' . $filter;
        }
        if ($orderby) {
            $dbquery .= ' ORDER BY ' . $orderby;
        }

        try {
            $sth = $this->dbh->prepare($dbquery);
            if ($sth->execute()) {
                return $sth->fetchAll(PDO::FETCH_ASSOC);
            } else {
                return false;
            }
        } catch (PDOException $e) {
            throw new \Exception('database query failed');
            die();
        }
    }
}
// end class CronJobPDO

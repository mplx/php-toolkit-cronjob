<?php
/**
 * Interface: CronJob
 * @version 2009-11-16 17:49:25 +0100
 * @copyright Martin Pircher <mplx+code@donotreply.at>
 * @author Martin Pircher <mplx+code@donotreply.at>
 * @link http://www.pircher.net/
 * @license http://opensource.org/licenses/MIT MIT License
 * @package Cronjob
 */

namespace mplx\toolkit\cronjob;

/**
 * Interface: CronJob
 */
interface CronJobInterface
{
    /**
     * Register job
     *
     * @param string $cmd command to be executed
     * @param int $interval interval in seconds
     * @param string $jobid unique job identifier
     * @return bool
     */
    public function registerJob($cmd, $interval, $jobid = '');

    /**
     * unregister/delete job
     *
     * @param string $jobid unique job identifier
     * @return bool
     */
    public function unregisterJob($jobid = '');

    /**
     * enable job
     *
     * @param string $jobid unique job identifier
     * @return bool
     */
    public function enableJob($jobid = '');

    /**
     * disable job
     *
     * @param string $jobid unique job identifier
     * @return bool
     */
    public function disableJob($jobid = '');

    /**
     * lock job
     *
     * @param string $jobid unique job identifier
     * @return bool
     */
    public function setLock($jobid = '');

    /**
     * release lock on job
     *
     * @param string $jobid unique job identifier
     * @return bool
     */
    public function releaseLock($jobid = '');

    /**
     * schedule job for next run
     *
     * @param mixed $ts timestamp
     * @param string $jobid unique job identifier
     * @return bool
     */
    public function scheduleJob($ts, $jobid = '');

    /**
     * get status of current job
     *
     * @param string $jobid unique job identifier
     * @return mixed array with job status
     */
    public function getStatus($jobid = '');

    /**
     * get all jobs
     *
     * @return mixed array with joblist
     */
    public function getScheduledJobs();
}
// end interface CronJobInterface

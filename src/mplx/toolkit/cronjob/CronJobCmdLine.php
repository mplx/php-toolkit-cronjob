<?php
/**
 * class for command line access
 * @copyright Martin Pircher <mplx+code@donotreply.at>
 * @author Martin Pircher <mplx+code@donotreply.at>
 * @link http://www.pircher.net/
 * @license http://opensource.org/licenses/MIT MIT License
 * @package Cronjob
 */

namespace mplx\toolkit\cronjob;

/**
 * Class implementing command line functions
 */
class CronJobCmdLine extends CronJob
{
    /**
    * command to be executed
    * @var string
    */
    private $command;

    /**
    * parameter (most times the jobid)
    * @var string
    */
    public $parameter;

    /**
    * Constructor
    *
    * @param mixed $dbconfig
    * @return CronJobCmdLine
    */
    public function __construct($dbconfig)
    {
        if ($dbconfig == null) {
            echo "ERROR: missing database configuration" . PHP_EOL.PHP_EOL;
            $this->command = 'help';
            $this->parameter = null;
        } elseif (count($_SERVER['argv']) == 3) {
            $this->command = $_SERVER['argv'][2];
            $this->parameter = null;
        } elseif (count($_SERVER['argv']) == 4) {
            $this->command = $_SERVER['argv'][2];
            $this->parameter = $_SERVER['argv'][3];
        } else {
            $this->command = 'help';
            $this->parameter = null;
        }
        if ($dbconfig && $this->command != 'help') {
            $this->initializeDB($dbconfig);
        }
    }

    /**
    * Process command line
    */
    public function process()
    {
        switch ($this->command) {
            case 'status':
                return $this->cmdGetStatus();
                break;
            case 'enable':
                return $this->enableJob($this->parameter);
                break;
            case 'disable':
                return $this->disableJob($this->parameter);
                break;
            case 'unlock':
                return $this->releaseLock($this->parameter);
                break;
            case 'delete':
                return $this->unregisterJobDB($this->parameter);
                break;
            case 'list':
                return $this->cmdListJobs();
                break;
            case 'table':
                if ($this->parameter == 'create') {
                    return $this->createTable();
                } else {
                    return false;
                }
                break;
            case 'help':
            default:
                return $this->cmdHelp();
                break;
        }
    }

    /**
    * Print full status of job
    */
    private function cmdGetStatus()
    {
        $status = $this->getStatus($this->parameter);
        if ($status) {
            foreach ($status as $field => $content) {
                echo $this->formatTxt($field, 16) . $content . PHP_EOL;
            }
            return true;
        } else {
            return false;
        }
    }

    /**
    * List (all|filtered) jobs and their status
    */
    private function cmdListJobs()
    {
        if ($this->parameter) {
            $jobs = $this->getJobsDB(array("jobid LIKE '" . $this->parameter . "'"));
        } else {
            $jobs = $this->getJobsDB();
        }
        if (count($jobs)>0) {
            echo "|" . $this->formatTxt('jobid', 10).
                 "|" . $this->formatTxt('enabled', 7).
                 "|" . $this->formatTxt('locked', 6).
                 "|" . $this->formatTxt('scheduled', 19).
                 "|" . $this->formatTxt('lastexecution', 19).
                 "|" . PHP_EOL;
            foreach ($jobs as $job) {
                echo "|" . $this->formatTxt($job['jobid'], 10);
                echo "|" . $this->formatTxt($job['enabled'], 7);
                echo "|" . $this->formatTxt($job['locked'], 6);
                echo "|" . $this->formatTxt($job['scheduled'], 19);
                echo "|" . $this->formatTxt($job['lastexecution'], 19);
                echo "|" . PHP_EOL;
            }
        }
        return true;
    }

    /**
    * output help
    */
    private function cmdHelp()
    {
        echo "CronJob Command Line Tool" . PHP_EOL;
        echo "-------------------------" . PHP_EOL;
        echo $_SERVER['argv'][0]." <dbconfig> <command> <parameter>" . PHP_EOL;
        echo PHP_EOL;
        echo "Commands and parameters:" . PHP_EOL;
        echo "status <jobid>           get status of job" . PHP_EOL;
        echo "enable <jobid>           enable job" . PHP_EOL;
        echo "disable <jobid>          disable job" . PHP_EOL;
        echo "unlock <jobid>           force unlock job" . PHP_EOL;
        echo "delete <jobid>           delete job" . PHP_EOL;
        echo "list [jobid|filter]      list jobs" . PHP_EOL;
        echo "table create             create database table" . PHP_EOL;
        echo PHP_EOL;
        return null;
    }

    /**
    * Helper function formatting output text
    *
    * @param string $txt
    * @param int $len
    */
    private function formatTxt($txt, $len)
    {
        while (strlen($txt) < $len) {
            $txt.=' ';
        }
        return $txt;
    }
}
// end class CronJobCmdLine

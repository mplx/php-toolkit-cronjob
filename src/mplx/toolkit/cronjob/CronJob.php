<?php
/**
 * Class for managing cronjobs
 * @version 2009-11-16 17:49:25 +0100
 * @copyright Martin Pircher <mplx+code@donotreply.at>
 * @author Martin Pircher <mplx+code@donotreply.at>
 * @link http://www.pircher.net/
 * @license http://opensource.org/licenses/MIT MIT License
 * @package Cronjob
 */

namespace mplx\toolkit\cronjob;

/**
 * Class for managing cronjobs
 */
class CronJob extends CronJobDBPDO implements CronJobInterface
{
    /**
    * actual unique job identifier
    * @var string
    */
    public $jobid;

    /**
     * Initialize job, connect to database
     *
     * @param string $jobid unique job identifier
     * @param mixed $dbconfig array holding database configuration
     * @return CronJob
     */
    public function __construct($jobid, $dbconfig)
    {
        $this->jobid = $jobid;
        $this->initializeDB($dbconfig);
    }

    /**
     * Register job
     *
     * {@inheritdoc}
     */
    public function registerJob($cmd, $interval, $jobid = '')
    {
        if (! $jobid) {
            $jobid = $this->jobid;
        }
        if ($this->registerJobDB($jobid, $interval, $cmd)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * unregister/delete job
     *
     * {@inheritdoc}
     */
    public function unregisterJob($jobid = '')
    {
        if (! $jobid) {
            $jobid = $this->jobid;
        }
        if ($this->unregisterJobDB($jobid, $cmd)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * enable job
     *
     * {@inheritdoc}
     */
    public function enableJob($jobid = '')
    {
        if (! $jobid) {
            $jobid = $this->jobid;
        }
        if ($this->setEnabledDB($jobid, true)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * disable job
     *
     * {@inheritdoc}
     */
    public function disableJob($jobid = '')
    {
        if (! $jobid) {
            $jobid = $this->jobid;
        }
        if ($this->setEnabledDB($jobid, false)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * lock job
     *
     * {@inheritdoc}
     */
    public function setLock($jobid = '')
    {
        if (! $jobid) {
            $jobid = $this->jobid;
        }
        if ($this->setLockDB($jobid, true)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * release lock on job
     *
     * {@inheritdoc}
     */
    public function releaseLock($jobid = '')
    {
        if (! $jobid) {
            $jobid = $this->jobid;
        }
        if ($this->setLockDB($jobid, false)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * schedule job for next run
     *
     * {@inheritdoc}
     */
    public function scheduleJob($ts, $jobid = '')
    {
        if (! $jobid) {
            $jobid = $this->jobid;
        }
        if ($this->scheduleJobDB($jobid, $ts)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * get status of current job
     *
     * {@inheritdoc}
     */
    public function getStatus($jobid = '')
    {
        if (! $jobid) {
            $jobid = $this->jobid;
        }
        return $this->getJobStatusDB($jobid);
    }

    /**
     * get all jobs
     *
     * {@inheritdoc}
     */
    public function getScheduledJobs()
    {
        return $this->getJobsDB(array("enabled='y'", "scheduled<NOW()"), array('scheduled ASC'));
    }
}
// end class CronJob
